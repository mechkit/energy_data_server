
export default function(){

  var db = sessionStorage.getItem('db');
  if( db ){
    db = JSON.parse(db);
  } else {
    db = {
      last_data: [],
      history: [],
      data_info: {},
    };
  }


  var init_state = {
    ui: {
      base_url: location.origin + location.pathname,
      home_url: location.origin + location.pathname + '#/',
      page_id: ['default'],
      title: 'Energy Manager',
      menu: [
        // 'summary',
      ]
    },
    admin: {},
    db: db,
  };

  return init_state;
}
