import {div, span, p, a, ul, li, br, h1, h2, h3, input, select, option} from 'specdom_helper';
import mk_titlebar_spec from './mk_titlebar_spec';
import $ from 'specdom';
import require_pages from './require_pages';

var context = require.context('./page/', true, /\.js$/);
var pages = require_pages(context);

var content_specdom = $('#content');

export default function(state, actions){


  var page = pages[state.ui.page_name];
  var page_content_specs;
  if( ! page ){
    page_content_specs = pages[404](state);
  } else {
    page_content_specs = page(state,actions);
  }

  var titlebar_spec = mk_titlebar_spec(state);

  var specs = {
    tag: 'div',
    children: [
      titlebar_spec,
      div({class:'transition'}),
      div({class:'background'},[
        div({class:'display_area'},[
          page_content_specs
        ])
      ])
    ]
  };
  // content_specdom.load(specs);
  content_specdom.clear().append(specs);
}
