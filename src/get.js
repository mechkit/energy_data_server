
export default function(url,cb){

  function reqListener (evt) {
    if( evt.type === 'load' ){
      try{
        cb(JSON.parse(this.responseText));
      }
      catch(error){
        cb(false);
      }
    } else {
      cb(false);
    }
  }
  var oReq = new XMLHttpRequest();
  oReq.addEventListener('load', reqListener);
  oReq.addEventListener("error", reqListener);
  oReq.open('GET', url);
  oReq.send();
}
