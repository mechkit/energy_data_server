import f from 'functions';
import reducer_express from './reducer/express';
import Chance from 'chance';

var chance = new Chance('SPD');

export default {

  init: function(state,action){
    console.log('reducer: init');
    return state;
  },
  set_page: function(state,action){
    var request = action.arguments[0];
    var page_id = request[0];
    var details = request.slice(1);

    state.ui.page_name = page_id;
    state.ui.page_details = details;

    return state;
  },

  set_data_info: function(state,action){
    var data_info = action.arguments[0];
    console.log('data_info', data_info);
    state.db.data_info = data_info;
    return state;
  },

  new_data: function(state,action){
    var data = action.arguments[0];
    state.db.last_data = data;
    state.db.history.push(data);

    return state;
  },



};
