/**
* this is the app
* @file_overview this the starting point for the application.
* @author keith showalter {@link https://github.com/kshowalter}
* @version 0.1.0
*/
console.log('/\\');

import 'normalize.css';

import hash_router from 'hash_router';

import get from './get';

import mkwebsite from 'mkwebsite';
import update from './update';
import reducers from './reducers';
import init_state from './init_state';
import mk_weather from '../lib/mk_weather';
import local_data_info from '../lib/data_info';

var state = init_state();

var global = window || global;

var actions = mkwebsite(state, reducers, update);

console.log('Working on:',state.ui.base_url);

const default_page = 'default';

get(state.ui.base_url+'data_info/', (data_info) => {
  if( ! data_info ){
    data_info = local_data_info;
  }
  actions.set_data_info(data_info);
});

var router = hash_router(function(selection){
  if( ! selection ){
    console.log('re-ROUTING... to default');
    selection = [default_page];
  }
  window.scrollTo(0, 0);
  console.log('ROUTING... '+selection.join('/'));

  actions.set_page(selection);
});

var server_connected = true;

setInterval(function(){
  if( server_connected ){
    var timestamp = Math.floor(Date.now() / 1000);
    get(state.ui.base_url+'mk_weather/'+timestamp, (data) => {
      if(! data){
        data = mk_weather(timestamp);
        data.source = 'browser simulation';
      } else {
        data.source = 'server';
      }
      actions.new_data(data);
    });
  }
},100000);

// setInterval(function(){
//   var timestamp = Math.floor(Date.now() / 1000);
//   get(state.ui.base_url+'status', (data) => {
//     if( data){
//       server_connected = true;
//     } else {
//       server_connected = false;
//     }
//     actions.new_data(data);
//   });
// },1000*60);


router();
