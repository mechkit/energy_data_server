import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, button, select, option, table, tr, th, td, col } from 'specdom_helper';
import {mklist, mkinput} from '../specdom_helper_extras';
import f from 'functions';


export default function(state, actions){
  var sun_spec = img(state.ui.base_url+'assets/icons/wi-day-sunny.svg',{class:'data_icon'});
  var cloud_spec = img(state.ui.base_url+'assets/icons/wi-cloud.svg',{class:'data_icon'});
  var storm_spec = img(state.ui.base_url+'assets/icons/wi-day-thunderstorm.svg',{class:'data_icon'});

  var values_specs = [];
  [
    'day_fraction',
    'high',
    'low',
    'rain_today',
    'sun_height',
    'rain_chance',
    'rainy_hour',
    'cloudy_hour',
    'cloud_cover',
    'air_temp',
    'irr',
  ].forEach((key)=>{
    if(state.db.last_data && state.db.last_data[key] ){
      var title = f.pretty_name(key);
      var value = state.db.last_data[key];
      if( value === 'true' ){
        value = 'Yes';
      } else if( value === 'false' ){
        value = 'No';
      }
      var units = '';
      var high = false;
      var low = 0;
      if( state.db.data_info && state.db.data_info[key] ){
        if( state.db.data_info[key].units ){
          units = state.db.data_info[key].units;
        }
        if( state.db.data_info[key].high ){
          high = state.db.data_info[key].high;
        }
        if( state.db.data_info[key].low ){
          low = state.db.data_info[key].low;
        }
      }
      var bar_spec;
      if( ! isNaN(value) && high ){
        if( high ){
          var color = 'blue';
          if( value > high/2 ){
            color = 'red';
          }
        }
        bar_spec = {
          tag: 'progress',
          props: {
            class: 'bar '+color,
            max: high-low,
            value: Number(value-low),
          },
          children: [
            value
          ]
        };
      } else if( key === 'rain_today' || key === 'rainy_hour' ){
        if( value === 'Yes' ){
          bar_spec = storm_spec;
        } else {
          bar_spec = sun_spec;
        }
      } else if( key === 'cloudy_hour' ){
        if( value === 'Yes' ){
          bar_spec = storm_spec;
        } else {
          bar_spec = sun_spec;
        }
      } else {
        bar_spec = span('');
      }
      values_specs.push(
        tr([
          th(title),
          td(value),
          td(units),
          td([bar_spec]),
        ])
      );
    }
  });

  var display_date = state.db.last_data.date.slice(0,4) +'-'+
                     state.db.last_data.date.slice(4,6) +'-'+
                     state.db.last_data.date.slice(6,8);
  var display_time = state.db.last_data.time.slice(0,2) +':'+
                     state.db.last_data.time.slice(2,4) +':'+
                     state.db.last_data.time.slice(4,6);

  var specs = div({class:'page'}, [
    div([
      span('Date:',{class:'small_title'}),
      span(display_date),
    ]),
    div([
      span('Time:',{class:'small_title'}),
      span(display_time),
    ]),
    div([
      table(values_specs),
    ]),
    div([
      span('source: '+state.db.last_data.source)
    ])
  ]);




  return specs;
}
