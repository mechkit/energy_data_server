import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, button, select, option, table, tr, th, td, col } from 'specdom_helper';
import {mklist, mkinput} from '../specdom_helper_extras';


export default function(state, actions){
  var history_spec = state.db.history.slice(-5).map(function(entry){
    return div([
      span(state.ui.page_details[0]),
      span(entry[state.ui.page_details[0]]),
    ]);
  });

  var specs = div({class:'page'}, [
    span(state.ui.page_details),
    div(history_spec),
  ]);

  return specs;
}
