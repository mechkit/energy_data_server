import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, select, option} from 'specdom_helper';

export function mkinput(actions, class_name, id, label, value, props){
  id = id || 'misc.input_' + f.name_to_id(label);
  // default_value = default_value || '';

  var specs = {
    tag: 'span',
    props: {
      class: 'input_group ' + class_name,
      onchange: function(e){
        actions.update_inputs(e.target.id,e.target.value);
      }
    },
    children: [
      span(label),
      input({
        class: 'input',
        id: id,
        value: value || '',
      }),
    ]
  };
  for( var name in props){
    specs.props[name] = props[name];
    specs.children[1].props[name] = props[name];
  }
  return specs;
}

export function mklist(actions, class_name, id, list, label, value, props){
  id = id || 'misc.input_' + f.name_to_id(label);
  class_name = class_name || 'input';
  var specs = {
    tag: 'span',
    props: {
      class: 'input_group ' + class_name,
      onchange: function(e){
        actions.update_inputs(e.target.id,e.target.value);
      }
    },
    children: [
      span(label),
      select({class:'input',id:id},list.map(name => {
        if( name == value ){
          return option({selected:true},name);
        } else {
          return option(name);
        }
      })),
    ]
  };
  for( var name in props){
    specs.props[name] = props[name];
    specs.children[1].props[name] = props[name];
  }
  return specs;
}
