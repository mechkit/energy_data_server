import f from 'functions';
import {div, span, p, a, img, ul, li, br, h1, h2, h3, input, select, option} from 'specdom_helper';


export default function(state){
  var title = state.ui.title;
  var menu = state.ui.menu;

  var selected_page_name = state.ui.page_name;

  var titlebar_specs = div({class: 'titlebar'},[
    div({class: 'titlebar_content'},[
      {
        tag: selected_page_name === 'default' ? 'span' : 'a',
        props: {
          class: selected_page_name === 'default' ? 'site_title site_title_selected' : 'site_title',
          href: state.ui.home_url,
        },
        text: title
      }
    ])
  ]);

  if( menu && menu.length !== 0 ){

    var menu_specs = div({class: 'menu'},[]);

    titlebar_specs.children.push(menu_specs);

    menu_specs.children.push({
      tag: 'li',
      text: '[',
      props: {
        class: 'menu_bracket',
      }
    });

    menu.forEach(function(name){
      var prety_name = f.pretty_name(name).trim();
      var href = '#/'+name;
      menu_specs.children.push({
        tag: 'li',
        props: {
          class: 'titlebar_option',
        },
        children: [{
          tag: name === selected_page_name ? 'span' : 'a',
          text: prety_name,
          props: {
            class: name === selected_page_name ? 'titlebar_link titlebar_link_selected' : 'titlebar_link',
            href: href
          }
        }]
      });
    });

    menu_specs.children.push({
      tag: 'li',
      text: ']',
      props: {
        class: 'menu_bracket',
      }
    });

  }


  return titlebar_specs;
}
