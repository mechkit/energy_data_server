FROM node:8


######################
# NOT CONFIGURED
######################





ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

RUN npm install --global pm2

#RUN mkdir /srv
RUN chown node:node /srv

WORKDIR /srv

ENV port=3300

USER node

#COPY package*.json /srv/
COPY . /srv/

RUN cd /srv
RUN npm install

EXPOSE 3300
CMD ["npm", "run", "serve_production"]
