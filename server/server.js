global.project = require('../package.json');

var fs = require('fs');
var path = require('path');
var local_path = __dirname;
var router = require('./routes');
var db = require('./db');
var Chance = require('chance');
var moment = require('moment');
var mk_weather = require('../lib/mk_weather');


var logger = require('winston');
logger.configure({
  transports: [
    new logger.transports.File({
      filename: 'server.log',
      json: false,
      handleExceptions: true,
      humanReadableUnhandledException: true
    }),
    new logger.transports.Console()
  ],
  exitOnError: false
});
global.logger = logger;
global.f = require('functions');
global.server_start_time = new Date();

// add timestamps in front of log messages
require('console-stamp')(console, '[HH:MM:ss.l]');

var express = require('express');
var http = require('http');

var port = process.env.NODE_ENV === 'dev' ? '3333' : '3300';
port = process.env.PORT || port;


setInterval(function(){

  var timestamp = Math.floor(Date.now() / 1000);
  var m = moment.unix( timestamp );

  var date = m.format('YYYYMMDD');
  var time = m.format('HHmmss');

  var wdata = mk_weather(timestamp);

  db(wdata);

},1000);

// Start express app
var app = express();

// Create HTTP server.
var server = http.createServer(app);

// Listen on provided port, on all network interfaces.
server.listen(port);

// Add routes
app.use('/', router);
app.use('/', express.static('public'));

logger.info('server started on http://localhost:'+port);
