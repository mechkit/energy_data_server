
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db/db.json');
const db = low(adapter);
// const path = require('path');
// const DB_PATH = path.join(path.dirname(__dirname), 'db.json');
db.defaults({ measurments: [] })
  .write();

module.exports = function(input){
  var out;
  if( input.constructor === Object ){
    // Add a post
    out = db.get('measurments')
      .push(input)
      .write();

  } else if( ! isNaN(input) ){
    out = db.get('measurments')
      .takeRight(input);
  }

  return out;
};
