var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');

var Chance = require('chance');
var moment = require('moment');

var mk_weather = require('../lib/mk_weather');
var data_info = require('../lib/data_info');

var local_path = __dirname;

var db = require('./db');

router.get('/help', function (req, res) {
  res.send('Nothing here. Try /api');
});



///////////////////////////////////////////
router.get('/status', function(req, res) {
  var start_time = new Date();
  var uptime_miliseconds = start_time - global.server_start_time;

  res.json({
    data: {
      version: global.project.version,
      uptime: f.format_milliseconds(uptime_miliseconds)
    },
    status: 'Running',
    time: ( new Date() - start_time ),
  });
});

///////////////////////////////////////////
router.get('/api', function(req, res) {
  var start_time = new Date();
  res.json({
    data: {
      Status_url: req.headers.host+'/status',
      API_url: req.headers.host+'/api',
      mk_weather: req.headers.host+'/mk_weather',
    },
    status: 'Running',
    time: ( new Date() - start_time ),
  });
});

///////////////////////////////////////////
router.get('/data_info', function(req, res) {
  res.json(data_info);
});


///////////////////////////////////////////
router.get('/mk_weather/:timestamp?', function(req, res) {
  var start_time = new Date();

  var timestamp = req.params.timestamp;

  var wdata;
  if( timestamp ){
    var m = moment.unix( timestamp );

    var date = m.format('YYYYMMDD');
    var time = m.format('HHmmss');

    wdata = mk_weather(timestamp);

    wdata.request_time = String( new Date() - start_time );

  } else {
    wdata = db(1);
  }

  res.json(wdata);
});




module.exports = router;
