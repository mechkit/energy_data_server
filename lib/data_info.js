module.exports = {
  'date': {
  },
  'time': {
  },
  'hours': {
    low: 0,
    high: 24,
    units: '',
  },
  'day_fraction': {
    low: 0,
    high: 1,
    units: '',
  },
  'sun_height': {
    low: 0,
    high: 1,
  },
  'rain_today': {
    low: 0,
    high: 1,
  },
  'rain_chance': {
    low: 0,
    high: 100,
    units: '%',
  },
  'rainy_hour': {
    low: 0,
    high: 1,
  },
  'cloudy_hour': {
    low: 0,
    high: 1,
  },
  'cloud_cover': {
    low: 0,
    high: 1,
    units: '%',
  },
  'sun_heat_factor': {
    low: 0,
    high: 1,
  },
  'high': {
    low: 50,
    high: 100,
    units: 'deg. F',
  },
  'low': {
    low: 50,
    high: 100,
    units: 'deg. F',
  },
  'air_temp': {
    low: 50,
    high: 100,
    units: 'deg. F',
  },
  'irr_ideal': {
    low: 0,
    high: 1000,
    units: 'W/m^2',
  },
  'irr': {
    low: 0,
    high: 1000,
    units: 'W/m^2',
  },
  'request_name': {
    units: '',
  },
  'request_time': {
  },

};
