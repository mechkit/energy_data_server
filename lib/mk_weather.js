var Chance = require('chance');
var moment = require('moment');

var data = require('./data');
// 'Month','Precip (in)','Min Tmp (°F)','Avg Tmp (°F)','Max Tmp (°F)'

var mk_weather = function(timestamp){
  var m = moment.unix( timestamp );
  var date = m.format('YYYYMMDD');
  var time = m.format('HHmmss');

  date = date || moment().format('YYYYMMDD');
  var month = date.slice(4,6);
  time = time || moment().format('HHmmss');
  var hour = Number( time.slice(0,2) );
  var min = Number( time.slice(2,4) );
  var sec = Number( time.slice(4) );
  var hours = hour + min/60 + sec/60/60;
  var day_fraction = ( hour + min/60 ) / 24;

  var d = Chance(date);
  var t = Chance(time);
  var h = Chance(time.slice(0,4));

  var precip   = data.month[month][0];
  var min_temp = data.month[month][1];
  var avg_temp = data.month[month][2];
  var max_temp = data.month[month][3];
  var dev_temp = ( max_temp - min_temp )/4;

  var precip_difficulty = Math.ceil(precip);
  var rain_check = d.d20();

  var rain_chance = 0;
  var rainy_hour = 0;
  var cloudy_hour = 0;

  var rain_today = ( rain_check > precip_difficulty );
  if(rain_today){
    rain_chance = d.d100();
    rainy_hour = ( h.d100() < rain_chance );
    cloudy_hour = ( h.d100() < (rain_chance+20) );
  }

  var sun_height = Math.sin( (hours +5 -12 )/Math.PI * 10/12 );
  sun_height = sun_height > 0 ? sun_height : 0;

  var irr_ideal = ( sun_height * 1000 );

  var cloud_cover;
  if(rainy_hour){
    cloud_cover = 0.9;
  } else if(cloudy_hour){
    var cloud_cover_mean = 0.75;
    var cloud_cover_std_dev = cloud_cover_mean/2;
    var cloud_cover_hour_mean = h.normal({
      mean: cloud_cover_mean,
      dev: cloud_cover_std_dev
    });
    cloud_cover = h.normal({
      mean: cloud_cover_hour_mean,
      dev: cloud_cover_std_dev/4
    });

  } else {
    cloud_cover = 0.0;
  }

  var irr = irr_ideal * 1-cloud_cover;

  var sun_heat_factor_ideal = Math.sin( (hours +5 -12 -2)/Math.PI * 10/12 )/2 +0.5;
  var sun_heat_factor = sun_heat_factor_ideal * (1-cloud_cover/4);

  var air_temp = min_temp + (max_temp-min_temp) * sun_heat_factor;



  var weather = {
    date,
    time,
    hours: hours.toFixed(2),
    day_fraction: day_fraction.toFixed(2),
    sun_height: sun_height.toFixed(2),
    rain_today: rain_today,
    rain_chance: rain_chance.toFixed(2),
    rainy_hour: rainy_hour,
    cloudy_hour: cloudy_hour,
    cloud_cover: cloud_cover.toFixed(2),
    sun_heat_factor: sun_heat_factor.toFixed(2),
    high: d.normal({ mean: max_temp, dev: dev_temp }).toFixed(2),
    low: d.normal({ mean: min_temp, dev: dev_temp }).toFixed(2),
    air_temp: air_temp.toFixed(2),
    irr_ideal: irr_ideal.toFixed(2),
    irr: irr.toFixed(2),

  };

  Object.keys(weather).forEach((name)=>{
    weather[name] = String(weather[name]);
  });


  return weather;
};


module.exports = mk_weather;
