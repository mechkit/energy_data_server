# Data display

The energy server includes a simple data live display.
This can be used as a system monitor, or as inspiration for a more complex display.

![Energy Manager Sample Full](./images/energy_manager_full.png)

![Energy Manager Sample mobile](./images/energy_manager_mobile.png)
